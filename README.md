# PeersTest

Project for Peers Solutions test problem.

We need to build a Learning Unit recommendation system.


## Algorithm

The algorithm is rather simple.
We have a very few data, that's why we use linear algorithm with some assumption: learning units should be different depends on industries and positions.

## How to run

You may create virtualenv with attached requirements (in file requirements.txt) and run Peers.ipynb with jupyter notebook or other appropriate programs.
This should work with python3.8+ (tested on 3.8).